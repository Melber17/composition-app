package com.melber17.composition.presentation

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.melber17.composition.R

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }
}