package com.melber17.composition.domain.repository

import com.melber17.composition.domain.entity.GameSettings
import com.melber17.composition.domain.entity.Level
import com.melber17.composition.domain.entity.Question

interface GameRepository {
    fun generateQuestion(maxSumValue: Int,countOfOptions: Int): Question
    fun getGameSettings(level: Level): GameSettings
}