package com.melber17.composition.domain.usecases

import com.melber17.composition.domain.entity.GameSettings
import com.melber17.composition.domain.entity.Level
import com.melber17.composition.domain.repository.GameRepository

class GetGameSettingsUseCase(
    private val repository: GameRepository
) {
    operator fun invoke(level: Level): GameSettings {
        return repository.getGameSettings(level)
    }
}